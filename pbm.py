#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# License: GPLv2

import argparse
import base64
import ctypes
import ctypes.util
import fcntl
import gobject
import gtk
import os
import struct
import sys
import time
import zlib


# Parameters
parser = argparse.ArgumentParser()
parser.add_argument("--interval", help="Check interval in seconds. Default: 30.", default=30, type=int)
parser.add_argument("--debug", help="Print debug info. Default: disabled.", dest="debug", action="store_true")
parser.add_argument("--themed", help="Load icons from icon theme (as opposed to using the embedded icons). "
                                     "In this case the icons pbm-0, pbm-1, ... pbm-100, "
                                     "pbm-charging-0, pbm-charging-1, ... pbm-charging-100 and pbm-present "
                                     "must be offered by your icon theme "
                                     "(you might find install-icons.sh useful). "
                                     "Default: disabled.", dest="themed", action="store_true")
parser.set_defaults(debug=False)
parser.set_defaults(themed=False)
args = parser.parse_args()

# Check interval in seconds
checkInterval = args.interval
# Print debug info
debug = args.debug
# Load icons from icon theme
themed = args.themed

libc = None
def readBatteryLevels():
  global libc
  batteries = {}
  if sys.platform.startswith("linux"):
    dir = next(os.walk("/sys/class/power_supply"))
    for d in dir[1]:
      if d.startswith("BAT"):
        # * name: string
        battery = {"name": d}
        # * present: bool
        try:
          with open(os.path.join(dir[0], d, "present")) as f:
            battery["present"] = f.read().strip() == "1"
        except:
          battery["present"] = False
        # level: float
        try:
          with open(os.path.join(dir[0], d, "energy_now")) as f:
            battery["level"] = float(f.read())
        except:
          try:
            with open(os.path.join(dir[0], d, "charge_now")) as f:
              battery["level"] = float(f.read())
          except:
            pass
        if "level" in battery and battery["level"] <= 0:
          del(battery["level"])
        # capacity: float
        try:
          with open(os.path.join(dir[0], d, "energy_full")) as f:
            battery["capacity"] = float(f.read())
        except:
          try:
            with open(os.path.join(dir[0], d, "charge_full")) as f:
              battery["capacity"] = float(f.read())
          except:
            pass
        if "capacity" in battery and battery["capacity"] <= 0:
          del(battery["current"])
        # current: float
        try:
          with open(os.path.join(dir[0], d, "power_now")) as f:
            battery["current"] = float(f.read())
        except:
          try:
            with open(os.path.join(dir[0], d, "current_now")) as f:
              battery["current"] = float(f.read())
          except:
            pass
        noCurrent = "current" in battery and battery["current"] == 0
        if "current" in battery and battery["current"] <= 0:
          del(battery["current"])
        # * status: string, usually "Charging", "Discharging", "Full", "Unknown"
        try:
          with open(os.path.join(dir[0], d, "status")) as f:
            battery["status"] = f.read().strip()
        except:
          battery["status"] = "Unknown"
        if battery["status"] == "Unknown" and noCurrent:
          battery["status"] = "Charging"
        # duration: int, seconds
        try:
          if "current" in battery and "capacity" in battery and "level" in battery:
            if battery["status"] == "Charging":
              battery["duration"] = int(round(3600 * (battery["capacity"] - battery["level"]) / battery["current"]))
            elif battery["status"] == "Discharging":
              battery["duration"] = int(round(3600 * battery["level"] / battery["current"]))
        except:
          pass
        if "duration" in battery and battery["duration"] <= 0:
          del(battery["duration"])
        # durationStr: string, h:m
        if "duration" in battery:
          m, s = divmod(battery["duration"], 60)
          h, m = divmod(m, 60)
          battery["durationStr"] = "{0}h{1}m".format(h, m)
        # percentage: int, level in percent (0-100)
        try:
          if "capacity" in battery and "level" in battery:
            battery["percentage"] = int(round(100 * battery["level"] / battery["capacity"]))
        except:
          pass
        batteries[battery["name"]] = battery
  elif sys.platform.startswith("openbsd") or sys.platform.startswith("netbsd"):
    try:
      buffer = struct.pack("BBBBIIIIIII", 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0)
      arg = 0x40004103 | (len(buffer) << 16)
      with open("/dev/apm") as f:
        buffer = fcntl.ioctl(f.fileno(), arg, buffer)
      if buffer:
        (batteryState,
        acState,
        batteryLife,
        minutesValid,
        minutesLeft,
        nBattery,
        batteryId,
        batteryFlags,
        spare1,
        spare2,
        spare3) = struct.unpack("BBBBIIIIIII", buffer)
        if debug:
          print batteryState, acState, batteryLife, minutesValid, minutesLeft, nBattery, batteryId, batteryFlags
        # * name: string
        battery = {"name": "BAT0"}
        # * present: bool
        battery["present"] = batteryLife > 0
        # percentage: int, level in percent (0-100)
        battery["percentage"] = batteryLife
        # * status: string, usually "Charging", "Discharging", "Full", "Unknown"
        if batteryState == 3:
          battery["status"] = "Charging"
        else:
          if battery["percentage"] == 100:
            battery["status"] = "Full"
          else:
            battery["status"] = "Unknown"
        # duration: int, seconds
        if minutesValid:
          battery["duration"] = minutesLeft * 60
        # durationStr: string, h:m
        if "duration" in battery:
          m, s = divmod(battery["duration"], 60)
          h, m = divmod(m, 60)
          battery["durationStr"] = "{0}h{1}m".format(h, m)
        batteries[battery["name"]] = battery
    except:
      pass
  elif sys.platform.startswith("freebsd") or sys.platform.startswith("pcbsd") or sys.platform.startswith("dragonfly"):
    try:
      libc = libc if libc else ctypes.CDLL(ctypes.util.find_library("c"))
      size = ctypes.c_size_t()
      buf = ctypes.c_int()
      size.value = ctypes.sizeof(buf)
      life = -2
      if libc.sysctlbyname("hw.acpi.battery.life", ctypes.byref(buf), ctypes.byref(size), None, 0) == 0:
        life = buf.value
      state = -1
      if libc.sysctlbyname("hw.acpi.battery.state", ctypes.byref(buf), ctypes.byref(size), None, 0) == 0:
        state = buf.value
      time = -1
      if libc.sysctlbyname("hw.acpi.battery.time", ctypes.byref(buf), ctypes.byref(size), None, 0) == 0:
        time = buf.value
      ac = -1
      if libc.sysctlbyname("hw.acpi.acline", ctypes.byref(buf), ctypes.byref(size), None, 0) == 0:
        ac = buf.value
      # * name: string
      battery = {"name": "BAT0"}
      # * present: bool
      battery["present"] = life >= 0
      # percentage: int, level in percent (0-100)
      if life >= 0:
        battery["percentage"] = life
      # * status: string, usually "Charging", "Discharging", "Full", "Unknown"
      if ac == 1:
        battery["status"] = "Full" if life == 100 else "Charging"
      elif ac == 0:
        battery["status"] = "Discharging"
      else:
        if state == 0:
          battery["status"] = "Full"
        elif state == 1:
          battery["status"] = "Discharging"
        elif state == 2:
          battery["status"] = "Charging"
        else:
          battery["status"] = "Unknown"
      # duration: int, seconds
      if time >= 0:
        battery["duration"] = time * 60
      # durationStr: string, h:m
      if "duration" in battery:
        m, s = divmod(battery["duration"], 60)
        h, m = divmod(m, 60)
        battery["durationStr"] = "{0}h{1}m".format(h, m)
      batteries[battery["name"]] = battery
    except:
      pass
  if debug:
    print batteries
  return batteries


class BatteryMonitor:
  def __init__(self):
    self.icons = {}
    self.timeLastCheck = 0
    self.timeLastTick = 0
    gobject.timeout_add_seconds(checkInterval if checkInterval < 60 else 60, self.updateTimerCallback)
    self.update()

  def rightClickEvent(self, icon, button, time):
    menu = gtk.Menu()
    check = gtk.MenuItem("Update now")
    check.connect("activate", self.update)
    menu.append(check)
    about = gtk.MenuItem("About")
    about.connect("activate", self.showAboutDialog)
    menu.append(about)
    quit = gtk.MenuItem("Quit")
    quit.connect("activate", gtk.main_quit)
    menu.append(quit)
    menu.show_all()
    menu.popup(None, None, gtk.status_icon_position_menu, button, time, icon)

  def showAboutDialog(self, widget):
    dialog = gtk.AboutDialog()
    dialog.set_destroy_with_parent(True)
    dialog.set_name("Battery monitor")
    dialog.set_version("1.0")
    dialog.set_authors(["o9000"])
    dialog.set_license("GPLv2")
    dialog.run()
    dialog.destroy()

  def makeIcon(self, name):
    self.icons[name] = gtk.StatusIcon()
    self.icons[name].connect("activate", self.update)
    self.icons[name].connect("popup-menu", self.rightClickEvent)

  def updateTimerCallback(self, *args):
    if debug:
      print "Tick"
    now = time.time()
    if abs(self.timeLastTick - now) > 120:
      # Reset timer after hibernation or clock change
      self.timeLastCheck = 0
      self.timeLastTick = 0
    self.timeLastTick = now
    if now + 1 >= self.timeLastCheck + checkInterval:
      self.update()
    elif debug:
      print "Next update in:", self.timeLastCheck + checkInterval - now
    return True

  def update(self, *args):
    if debug:
      print "\nUpdate"
    self.timeLastCheck = time.time()
    batteries = readBatteryLevels()
    for i in batteries:
      battery = batteries[i]
      if battery["name"] not in self.icons:
        self.makeIcon(battery["name"])
      self.setVisibility(battery["name"], battery["present"])
      if "percentage" in battery:
        if "status" in battery:
          if battery["status"] == "Charging" or battery["status"] == "Full":
            self.setIcon(battery["name"], "charging-" + str(battery["percentage"]))
            if "durationStr" in battery:
              self.setTooltip(battery["name"], "Battery {0}: Plugged in, level {1}%, time left {2}".format(battery["name"], str(battery["percentage"]), battery["durationStr"]))
            else:
              self.setTooltip(battery["name"], "Battery {0}: Plugged in, level {1}%".format(battery["name"], str(battery["percentage"])))
          elif battery["status"] == "Discharging":
            self.setIcon(battery["name"], str(battery["percentage"]))
            if "durationStr" in battery:
              self.setTooltip(battery["name"], "Battery {0}: Discharging, level {1}%, time left {2}".format(battery["name"], str(battery["percentage"]), battery["durationStr"]))
            else:
              self.setTooltip(battery["name"], "Battery {0}: Discharging, level {1}%".format(battery["name"], str(battery["percentage"])))
          else:
            self.setIcon(battery["name"], str(battery["percentage"]))
            self.setTooltip(battery["name"], "Battery {0}: Level {1}%".format(battery["name"], str(battery["percentage"])))
        else:
          self.setIcon(battery["name"], str(battery["percentage"]))
          self.setTooltip(battery["name"], "Battery {0}: Level {1}%".format(battery["name"], str(battery["percentage"])))
      else:
        if "status" in battery:
          if battery["status"] == "Charging":
            self.setIcon(battery["name"], "charging-100")
            self.setTooltip(battery["name"], "Battery {0}: Plugged in".format(battery["name"]))
          elif battery["status"] == "Discharging":
            self.setIcon(battery["name"], "5")
            self.setTooltip(battery["name"], "Battery {0}: Discharging".format(battery["name"]))
          else:
            self.setIcon(battery["name"], "100")
            self.setTooltip(battery["name"], "Battery {0}: Present".format(battery["name"]))
        else:
          self.setIcon(battery["name"], "100")
          self.setTooltip(battery["name"], "Battery {0}: Present".format(battery["name"]))
    for i in self.icons:
      if i not in batteries:
        self.setVisibility(i, False)
    return True

  def setVisibility(self, index, value):
    if debug:
      print "Visibility:", index, value
    self.icons[index].set_visible(value)

  def setIcon(self, index, icon):
    if debug:
      print "Icon:", index, icon
    if themed:
      self.icons[index].set_from_icon_name("pbm-" + str(icon))
    else:
      self.icons[index].set_from_pixbuf(images[str(icon)])

  def setTooltip(self, index, text):
    if debug:
      print "Tooltip:", index, text
    self.icons[index].set_tooltip(text)


# Images
# Generated with http://bre.klaki.net/programs/img_to_pygtk.py.txt
# from the battery icons of the xfce elementary theme:
# https://github.com/shimmerproject/elementary-xfce

images = {}
images["0"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrVl21Mk1cUx1niksVkH/yCyT7MhJjppp2iBha2xDEcGWAgC1sAcW5IzEQaQ6AKS10kJFsCowviEi'
'3jpaVvdFBaWiigvMo7lHdKW0pLQeeKOodDt6yOnv0vaT9Y+eL67MOe5CTPc3vv/d1zz/+cexsS8mJP'
'Y2NjqF6vtxiNxr9bW1u9zNg72mabm5t3hPwHDxG9VFZWlnjlypVrTU1NDwcHB2loaOgZY21arfYe6y'
'MSiRLZGC7YmCtKKpWuqlSqdfjpho+P+vr6yGw2k8Vi2TT2fuvWLW9LS8tD/O5mfSUSiZuNDZYvl8vX'
'wL0Nhm1mZsZ848aNOwqFwjs8PExVVVVUW1tLo6OjJJPJNhCLJbSbp6enbXi/jbbfguW3tbX9PjU19U'
'tXV9d4d3f3WEdHx0Rpaen9/Px8D5/P9zIrKCj4q7i42A0NmGBj7e3t4xjjxthHwfIFAoF6aWlpDf4/'
'Hh8fv4s4L/b398/29vZOYi2MNQbOJGwW34vw/+7c3NxjNiYvL68uWH5KSooY+jL09PRosYbRhYWFZa'
'fTuY75PcvLy8QM3x673b4On5egg0HoQIOYGNhYLvjwWX/z5k2p1WptB7fd5XK1gMXaxfBZjD0X19XV'
'iaurq8XsQdzF4Ou54mO/mxgfWm9bXFxsW1lZMVZWVv6UnJw8lJiYOFxSUlIXyEccmrjiI9+0iLN0fn'
'7eCL4Re96cmZk5GB8fb4qNjTWlpqYObMHXcsWH1hqRdxLEvwX8Fuy/IScnZ9TPz8jIGA3kIzaNXPGh'
'PQ38l0DXBujPAA00FRUVDfv5ubm5I1vwNVzxkfcNyK+a2dlZPTSoB19XXl7e5+cXFhY+t//QTANX/M'
'7OznrGR/3Tga9zOBxa1OQuPx91tieQjzys55CvRj2tQc5pwdeCr8E5ZPTzgWwP5EMzaq74yD01/K8G'
'X4Mc0KDWNDBN+Pk4D/SBfPzOGR/aV+EMqkb9bYAGG8Cvn5yclCUkJGzyce6qA/nQjIorPnxXMr7JZK'
'oHvx58NbT4I/LeFBcXZ2KxCeTjvFJyxQdbAatCTVVDg2qbzVbH6m92dvYYW4PBYKgI5EMzCq74uFPI'
'YYyvwtmuAl/J+EKhcOTcuXNjW9V/aEbOIV+GM60SNVWJuCtxDigYH3fBXtxPBrbio17JuOJjf6WMj5'
'qmAF8BvpzxoQH1xMREw1Z8aEbKFR+5LsEeVA4MDMiQAzLkYC3j446nvH79ulKn0z0Xf2hSwhUf+VXT'
'IxJpZy5eHLOdOPGz49ixP38IC3uaHRrqZVa+a9dTa3j4H1PR0SvdaWmDzZcva7BfNVzwBQkJaktMzC'
'NbRITHlpy8YTt1iqxnzpAgIoKyeTzi79tHFw8epIX4eLIfPUq2yMgN24EDHmt09NqF48eDvX+9MsXj'
'PbGkptJcVhbN8/lkOXuWrJmZ1CUU0tdRUXQpMpJ6BAKyx8U9Y46YGO/0/v1P2Bz/kv0ybKcz4zNazP'
'qSzKdPkzUjg5j/trQ0cp0/T/cqKui+WEzLWNszfOyFKy2FHJ+nE5vDN9cL+w57Tf5+7F1noZDcpd/Q'
'7Uv55OJnkeNkOjmSEjc5zBxJSeT49BNyZX5Bdy7kkLukiFwYI4v+8A6bI4g92HFkz553ReHvmA3vfb'
'AxfPLkhvMrAa2KvqUHV0vo12vfb9qDq9/RKtbnzM+jkfT0DdZXFB45x8ayOYLUwHbmw0dvvPlxAe+w'
'5CrviF361qF1+d6DHuXut73M5HvDPaytfP9hez7vUA3r6/N7O4d/Q7fBXvXF83VYGGy3z8J8bTt9fb'
'aF/E+efwDqubIe'
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["20"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrVl21MU1cYx1niojHxg19moskSVFQmIMwJog4RAYUScEOHjglBNnWBIQoMZqYyXJbUSHyZ07AB8g'
'61lNIWCuW1CBaEVt6htxQKSBA1cTBwChGe/Q+BD1a/uN592E3+ybmn95zfec7zf84BK6t3e4qKij6Q'
'y+W9SqXyVVlZ2RwTa6Ovs6SkZKXVf/AQ0XtXr171v3bt2i2ZTPZMo9FQY2Pja2J9Uqn0CfsmOTnZn4'
'3hg425dmRmZj7Oz8+fRJxjiHGivr6euru7qbe3d16sfffu3bnS0tJn+H2MfZuRkTHGxlrKz8nJGQf3'
'IRhcR0dHd0VFxUhubu5cU1MTpaWlUVZWFjU3N1N2dvYscmFCf3d7ezuH9kP0/Wkpv7y8/K+2trZHNT'
'U1utra2paqqqoHly9ffhofHz8TGRk5x5SQkDAtFArH4AEt1KJSqXQYM4axE5byY2NjRSaTaRzxT+l0'
'ulHk2djQ0NBZV1fXirUwVgs4rVAn3o2If7Srq2uKjYmJiSmwlB8UFJQCfynUarUUa2g2GAxDAwMDk5'
'h/ZmhoiJjwPtPX1zeJmE3wgQY+kCAnCjaWDz5illdWVmbq9XoVuKrBwcFSsFh/CmJOwZ6nFBQUpKSn'
'p6ewB3lPAV/OFx/7LWN8eL3caDSWDw8PK1NTU+8EBgY2+vv7N126dKnAnI88yPjio96kyHNmT0+PEn'
'wl9rwkPDxc4+vrq/X29tYePnz43lv4Ur748FoR6i4D+S8FvxT7r4iOjm5e5IeFhTWb85GbIr748J4E'
'8WfA1wr4TwEPyJKSkpoW+WfOnLn/Fr6ELz7qvhD1dbuzs1MOD8rBL75+/Xr9Ij8xMfGN/YdnCvniV1'
'dXixkf518x+MX9/f1SnMk1i3ycs2pzPupQzCNfhPP0NmpOCr4UfAnuIeUiH0iVOR+eEfHFR+2JEH86'
'+BLUgARnTSHzxCIf94HcnI/feePD+/m4g9Jx/hbCg4Xgi1tbW7MFAsE8H/euyJwPz+TzxUfseYyv1W'
'rF4IvBF8GLf6DutT4+PlqWG3M+7qs8vvhg50JpOFNF8KCI47gCdv5GRES0sDUoFIrfzfnwTC5ffPxN'
'kVN35UpxT1LSA+Px449MAQEvDI6O0zddXWeFjo6znIPDtN7J6e+2PXuGa48c0ZRcuCCBZ3L44McKBC'
'LOz2+Cc3WdMYaEvOo7eZIMUVHUHRxMNTExVBEdTdyBA2Tw9aW+3bvJsH37K27Llhm8j8f5+Vl8/7bb'
'20/pQ0OpKzKSuLg4Mpw+TQa0e4OCaADrGDx1igwCAfX5+Lwmk0Aw12FnN2UpfyDsKA1ER1FPeDgZIi'
'Ko78QJMqKtDwigGg8PUru7E7d//xv8obBQ6g8NJkv5Mmc3GhVepMfJv9BI0nkajj1Npq/D6ZaLM31n'
'YzOvm87O1H8wkAaPhdJITBSNCX+iR8Kfqdh5l8X8vPUOdN/Tmxr2eFHPieM0cvEcPb2RTImB/vS9YN'
'+8WPsJ1vfwbBx1hXxFdbt2U6O7J7GxFuKXsTmaPDypYvPHVOfqRjr4jDsSRPcSz5HQ14uEAm/SYF+4'
'g5+RzmMvqZ22UZmNHWncPBb5y/4l+31oFeaYu+fmTpX2W0m9bSdp9+0j/RcH6fH5BHpRfIdeKiT0JP'
'Hsa/zyjQ5Uv8ON8efYHAtzvXPs0OrUdXY1RZu3vlQx/ic7qMXLi/SHAmnkWDCNx3xLk/GRNPpNCHGH'
'PifdXk+q2+pCyk0OJLF1epm6zr6KzWHBHqxcvXTpxhvWH4kRy7TY1ul5lcsu0sHfA0eD6Bn4E+APhn'
'5JrQJfqnbZSeJNW57nrbef/tXa9g4by+aw0APLWQyfrli58+yaded+s7ZVZ9nYj+ZucJjCmmbntcFh'
'kvXdtLat/WGN9Y/s24W4l/P4b+gSaMVCPj+E1kLrF7R2oW/VwjdLrP4nzz9E35yx'
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["40"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrVl3tM21UUxzGZiWL8Y5kRBaITGTBAGSpM3NwUIgrLFqNZ8MHCQ9yj7Vi2llEfMDCwDGiRglrKgL'
'7oKEIp0Afv8rTlVUDKK7xhOkHjmAMNkI7juT/JksVEmL9K4k2+ye3t/Z7PPeee36+pnd39jfLy8ser'
'qqpG9Hq9tbq6ep2IzHHNotVqd9r9BwMAHsjKyjomEAiElZWVN41GI5hMpntE1tRq9c9kD5/PP0Y8tm'
'BjrFekUulCcXHxUm1t7YJOp7vd1tYGQ0NDMDIyQonMW1paoKamZhHrQe2VSCTzxEuXX1RUdAtjXh8e'
'Hh4bHBwcqqur+1GhUKx3dHRAQUEByGQy6OrqArlcfgfvYaazs3PYYrGM4Vmu49oiXT7Gud3T03PDYD'
'CYm5qauhsaGnp5PN4v8fHxaywWa52Iy+WupqWlzWMP9KC6sU5m9PyE3t/o8jkcTsn4+PhNrPGy2Wy+'
'gfc80d7ebsF69+FZCKsbOX0oC36ewLrcwDoto+dXNputpMsPCwsTYV/Lm5ub1XiGrrGxsdmpqaml6e'
'nptdnZWSDCz2vIW+rv759ubW01Yo+o8Fxy4rUVv76+Xjo6OlqL3NqZmRkdskRYCxHmLMKai5RKpaiw'
'sFBEBt67yJZ8jUYjI3zs9ZqJiYmaubk5/WZ87FOZLfmYjxSfAT3y9Vhz7XbyMb4M40nw/nXI12H9NZ'
'vxsR9tysf8JdjXGuw/DfZA5XbzMZ4Y3ytV2INVyK/YTj7ev5TwBwYGKpBfMTk5qd4CX2pLPr6DxchU'
'I1+NfNV28vE3T5pVVFGZrqz//lyebiGMp1rxjs1d9WAIre6Mb6yeLOGqd6zwj4NxornjiUJjQrZYZS'
't+6IcflwQnym4d5IrXWFdr73DkrRB3zQTv5jTCUUEzBPIM4J9aB15JeniWqwaXCwqrG0u0FpggXQz9'
'IIb2+9eLmb38kbAaOAojJKrMEFfSA4yiLgjKbIKANAP4pDSC66UGeDqh/h69kKxddz8lWKadf5YBTk'
'qMEJnfDkzkRks6IeyqCfalNoJb0t+5lBLr4XBmKwRnGoAuP+CSFmKUg3CybBSYZSNwQtYPR4Rd4Jpk'
'oDhEe5IN4JnaAv58E7yZ2wvhxSMQUzIELydqaPOf45SAb5wMvC7kw/GvG+G00gJs7TTElE/B6coZSm'
'QeWToO7xT0wqHLWnA9K6I8xEv7/s8rqVhOUTzwuyiG17+ogCP8Fgj60gxvF8ygZqm5f3Id7GUXg3NM'
'DjwWfuWvM6OXLn/vuWuwjyMDx8gM8GUXwqEkNbyV0bwpn3iIly7f/aziLv/58/lwIFEFwelNW+ITL1'
'2+K1NOxXoiIh08Y/Ng/2dlEHTFsCU+8dLEP+RyRgo+bCnF98C+8vu0FF67vDmfeIiXxPiX7AdRDrtP'
'ialeco7KAFeGEHy5JfBqSsM/8qleQQ/xkhgbse47d5TjM9G5U+5MidWb9RW8yCmEgM9VVP5vZPXe5Z'
'P5ATzTS1gbH+wRT2YO7GGIrbujhBMkBo0a7HzYK9Df6UR2p3O0aMWDWbCy/5MyCM0wwPt5fXDm2x8o'
'kXlIRiP4cUvBnZG/QvY6hgtM9m6H/UgMmj1gT3J4xOdo8K6Q+FSH9/jfPRkhnHeKyvvdMSJ/nVJk3j'
'JZI9/tCrmYQvZu5G1vw7+hO1CPbtznUygX8nhsyGVjzWFjzw67/8n4E7hbtA4='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["60"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrVl3lMVFcUxmlik9amfxib2iJRS1EUrSgK1tqKW10jMTXVLjYCxYiANjosU8ARGyAijDJIHIfAwM'
'ywjSwDzALDMqwOsgyQjIACYbO1aFOxShskyNdzX4iJaVKwb9qkN/mS++673/nde+65L3l2di/XCgsL'
'3y4pKek2GAyTpaWlU0ysT2NWnU43z+5faABeSUxM9JJIJNLi4uKHZrMZjY2NL4iNaTSaB2yOWCz2Yh'
'5bsCnWRwqF4n5OTs4To9F4X6/XP66vr0dnZye6u7s5sX5tbS3KyspGKR/c3IyMjBHm5cvPzMx8RDHv'
'dnV19dy6dauzvLz8p6ysrKmbN28iLS0NSqUSzc3NUKlUz+gcBpuamrqsVmsPreUujY3y5VOcx62trf'
'dMJpOlurq6pbKysi0hIeGXsLCwiaCgoCkmoVD4NC4uboRqoJXUQnmykOdn8v7Glx8cHKzu7e19SDke'
's1gs9+ic+xoaGqyU73ZaC2O1EKedZKXnPsrLPcrTGHl+FQgEuXz5hw8fllFdq2pqajS0huaenp6h/v'
'7+JwMDAxNDQ0NgoucJ4j3p6OgYqKurM1ONFNC6VMxrK35FRYXi9u3bRuIaBwcH9cSSUS5ktGcZ5VyW'
'm5srk8vlMtbo3GW25Gu1WiXjU62X9fX1lQ0PDxtm4lOdKm3Jp/0o6A4YiG+gnOv+Sz7FV1K8DDp/Pf'
'H1lH/tTHyqR5vyk7NLSpKLajsjlBUP/JK146tOXXu6PEA66RxwddIlSPp01SnpHx+HyIY/F0nNZ5PS'
'C2zF3/v1MfXuKNUjz3DFRKjS9EykNkOU34yDV6qwX1KDbQkmeMSUY2WUAe8LNXA8kzW5LEg2sUOkHN'
'37lR/v+7cyMGnMT1qK0JxGxGo7INK0Q3Ddgu2XqrExzgTX6Co4navE4rMVL2jded2U83HJGF/+PokJ'
'/hlm+MobEHK9DSezLfBRtGBNTBWWRf2Vy0lUga2X67DrcjX48rfEGnEstxP+BXcQUnwH/morDsktcI'
'oycRympedNcImphYe4EbuuteFITjeOqbvgGVPGm+8RWQS3UBVWB8vhk1qHM4VdiDAOwa+wH/7Fg5xY'
'3zuvF5+ltWFzrA5OJ2Wch3n58teHF3KxFvtdwrZz2fC6qMeh5BvYftmCA2mDpCGu73G+HCsEOXDwu4'
'K3jlzgPMzLl+8mLIBbiBKLvhXDMzIT+y7ocDCpYWY+eZiXL39NaB4Xy8E3AZvCldgVq8WBxPpZ8ZmX'
'L/+DYDXWUqyFPgn4MEyBHdHF8KLanonPPMzL+/6fzn3Odw9Nx9YfirBPXDsrPvPy5a/4LhtrgpWw94'
'7HWoEcm6M02B1fMyOfeZiXL9/5ZNZz/urTqdgkKsDOi9Wz4jMvX75ToIqL9c7Ri3A5lYINEfnYfsE0'
'Kz7z8sS/5nhCAVeBguMvp++Ke3getsTOzGce5mUx/iH7VdKCJcfTuVpy8ImHU4AUa4VqfBJd+bd8rl'
'bIw7wsxnSsl947yf4932v9zoEZk6uCkrGOvsEbIwu4/X+a2Pacz/qbaE3rKTeuVCMugVewNCB9comP'
'tI/F4JGDea+v3Oax8JukJgdf2fjywLTxDd/nY2+8CV+mtOPE9R85sf6e+Cq4C/PgHJA6zubaH5E0zl'
'3m6c5i8KyBuWwPb7ju3zl/T1jMgi/EN949Kh1Z6JPyu/3R1ClO3iljbIy9m78nNJrNnd73XBv+hs4h'
'vTl9notIjux6TMtxemzB9Jw5dv+T9idtSaU/'
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["80"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrVl3tMU1ccx1niksVkf5gsc3EorPJQ2aY8lRF1ylRAh0zNiIoKCDgezjgQOoYICii0QHlMxFmlFK'
'jlVYpQoUILAymvQmaBLuAYTMZ0br5GTEaw351zQ0jMklF33B+7yTc5PTmf3+ee3z3tTS0sXuyqrq5+'
's7a21qhSqWauX79uoqFjMmeoq6tbZPEfXABeEYlEvjk5OQVKpfJBR0cHdDrdc6FzCoXiV7omMzPTlz'
'Ivw01qfSCRSO7JZLI/Ghsb79XX1z9pa2vD4OAgjEYjFzpubW1FQ0PDQ9IPbm1RUdFdyrL6S0pKHpGa'
'd4aGhoYHBgYG1Wr1z6WlpabOzk6IxWIUFxeju7sbUqn0GXkOY11dXUMGg2GY3MsdMveQ1U/qPOnt7Z'
'3UaDR6rVbb09TU1CcUCu/HxcVNR0VFmWj4fP6f6enpd8kZ6CXpIX3SE+YXwj5m9cfExMhHRkYekB5P'
'6fX6SfKcb7e3txtIv/vJvVBXD/H0kxjI59ukL5OkT1OE+T06Ovoqq9/f37+QnGuptEatrdR0/VR/s+'
'9pr+F7k3H4NsbHx7kMkTGdU7R0PxUrm38skCmbyX1JKcvq99kfKvdOkj72PCmdTqnoeCao7UWqUo/d'
'ec34OKcFm4UauKWq4ZCkwnK+ArwvSmfsogqntyZKH/nsC2Hev0Nk7lTohQbwr3YiVz2ADNUAkpS34J'
'mlhXu6BqtTmmFzqglWJ288F9fTdSb7IzlTrP4duVqES3QILepAcu0txFd/h2PyfqxJbYZd0t+9XBJv'
'wFPUBi+RFsz9z25BmHwQkYphnG78AbFKI0LLbsEmScN5aGyTNViV2gq3TB22XehDgMyIsPIheGez+z'
'edbYBLXAmc44pwrFSHU6phpGkmEFI9is+UY1zoOLBiBLvEfdiQVgebo4UcQ1lW/8ZUFVdreVg2dmVU'
'4UB+E0LE3fDM1sNPPEYyzo3dktVYGS2DZUge3gg4xzGUZfWvP1MP51gpeMS/82wl9uWqEfRN17x+yl'
'CW1e+RfI2r9U5oNnaklsNf1IiDhZ1m+SnL6l+XqIQTqWUdkgXvM3LsyWrA/gLdvH7KUJbV75ZQw9Wy'
'Iv4tyTL4CVXYe77DLD9lWf0u8dVz/s2nyuCbUY9P82+a5acsq9+JXwWnE8VYdjgTGxNKsP1cHXbnts'
'/vJwxlWf1rYiu4WpbBQnjEF2Nb2jX4kd82c/yUZfW/FyOHI6n1dpAQ6+Ik+ChFCd/sb+f1U4ayzO+f'
'41fn/K6xV7DpdA22Z7aa5acsq3/lsTKsiSnGkkABHKMvY0OSAl6Clnn9lKEsq9/+aOmc//3jl+CRWI'
'WtGVqz/JRl9dtESrlabx3KwKrPL2LtV5XwPKcxy09ZRv1rvHAJVkdLOP8K8l5zja/Ah2nz+ylDWVrj'
'X7pfJVlsfeQKd5YsgwSwiSiAI1+O9SlN/+jnzgphKEtrzNZ64b2TLLEL/vo3+8gi07tR+XCOuQz3hC'
'pu/1tEfXN+OvYg9+RCerOanJFVkXmwi7hisj2cf5/WYOjBImt3Hy/bQ1kTS4MLZxwiLs+s/bISPgIN'
'9l7sR3j5BBc69hY0w5VfgRXh4hm61uZg1h1rN69ttAbjGVhI98DbsGe/rd+JMt4B4ahV4PmpZcEXp5'
'cEXjLRLA0qnKZzvADBqO0nJ0ro2tl9L3yJf0MXkLw++zyXkfDo12M2vNm5xbNrFlj8T66/AGpHmxw='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["100"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrVlltMU3ccx9ke5gMxWyLZFFCcoFwcsVyEITiZDBg4BsOpYUMoyJACikgndxjjLi0tVFiBFihUkE'
'uBAlbHpXKT4ubqHiRsMVncsmzvcw84M777/46dyV7W6nEP+yff5J//OZ/v5yTnn3P+dnZPN3Q6nbNe'
'r//JYDBssMCSDbb2K7u2y+4/GgFlV44FFvdPSrXjD5eXl2Eymf4RWpP26Nf3F2kHAkr64p6XV5Dfe8'
'A7V/2bn7hrI7XFgOa+CSwtLWF1dRVra2tcaL7I1so7xxFWOwGPc2p45qge+Ig1QXz9e3Pa/8jsmEbr'
'zF18PmZGleYqtJf7sbKygq6uLi4079H2QdQyiRjlLST1mBFcbYDH2Y6HfP0xijmUj95BsmoRsWweKZ'
'nGR4VSZJ4vQGZWNkQsGbn5iBNL4FpyDS6l03CvnEWy5jaim+bA139UaULe2HcQDqzhaOe3OKxYgaBu'
'AW6fGeFSNs1ld4URXtXzCJCaEKk0I7F/DXn67xGvXObtj2meQ0CBFl65KhyRTeF4pxnCwXtIG/kBGf'
'r7XGguHLqHeLUZb9VMwu1MG8cQy9cfLTdyXXtETUhunoBINQ9R922Eyb5BnPo+y4/cPKBiCp55/XBO'
'U8AhsY5jiOXrf7dxFvvzmT+jCYnycaS330B2j3U/McTy9Uew/UZduzPkSGgcwymlERndX9vkJ5av/5'
'36KfizLrfTchyXjELYOov0Tut+Yojl63+79jrX5ZouQ/xFHU5emkGa+iub/MTy9R9i35G//XF1w/hY'
'MY2Ujls2+Ynl6z9YeRV+F3rx+icyvFc9iBPyL5HUtmLVTwyxfP3BFRNP/FFVAzgmu45E9k2yxU8sX/'
'+bZXr4sq6daY2IqOjHB9JrSGhdtuonhlje/92SMa7LhfnDyvsQ22DAiZabNvmJ5ev3LxrhunackiK0'
'VIsj9ZP4ULFkk59Yvn7fAh18P+3B9lQpDhb3Ior93+ObFq37GUMs7/PHhSGuyzlVgqBCDSKqxxErt8'
'1PLF+/t3gAPqzLKUWCwPxuhFXqEdO4YNVPDLG8zz+5V7guR2ED/MSdCK0YRbRk3iY/sXz9njl9EIgf'
'+wXn1QgpH0HkxTmrfmKI5et3P3OZ69qWfBHe5zpwoFSH8PobNvmJ5et3y+rlurYyv+fZdgQUD+Nwrd'
'EmP7E89S/vEmmwL0+D15Lq4Z7dBv/CIRyqmbXqJ4ZY6nhG9yYWx52nux7vf/b+XUVfwCd/ACFVM//q'
'p71CDLHUYel62vESy7YdqW2r7tndj97IvsTt/6ASHUJrjAiXm5/4aR7Mnsm/aAj72DnVK0uBPZndj7'
'anKO9Sh6XrWYa9vSBa4JSkMDmntq17ZKnXAwuHEd1gREL7HYgGf+ZC86gGdk4sGIJ7pmqd7nU82XzT'
'fm+4gDp47oEXWV7ZHJgQ4hBTWrY1QbbgmKz8xSml43dHoepPLintD2iNrjm8X1JK9xJjYZ/neMHyLj'
'ezbGF51ZItlrVNlnv+N+Mv1k9sxg=='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)


images["charging-0"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrNl2tMk2cUx82SJSaafdn2xS/LnG6iCOJQmQk0a0WRFvyE3AQR610BYZiZbRm6BDHoYEo2R2XcpN'
'CWS7mW+1WgoyhQ6L2FcnUS0LjNRYV69n9MzZJlZpTXJXuSk7x93/c5v3PO83/O83bFCtdGUVGRn1Kp'
'vF9XV+dQqVTPmbHrysrKeyUlJT4r/oNx5cqVPdeuXcvKzs7uBdvR29tL/2R4tsjeYe+yOVy5V69eDc'
'zLy7svlUqfIL+5tra2e7du3XJoNBoaGRkhg8Hwwti1Wq2m4uLiZy0tLfcQxxybk5ub+0tGRsbe5bBT'
'UlJWw8fT2traKfg3wQxDQ0PD4C9UVVURYiGJREJgUFdXF5WWlhKePe7u7h6+c+eOYXBw0IR1mWJxpK'
'enr1oGfw1yeTowMDAL/4Otra2ajo4OjVwuN+LZ7/Hx8Y6TJ08Ss4SEBAfuPYI2dIhNU1NTo0F8CGFw'
'trm5+Snz5So/Ojr6bcxTjI6OPkSNWRxzfX19U1hnc09Pj66zs1Pb3t5+F/7vNjU1aWE6xGu+ffv2VH'
'9//xybY7fbHzIfzNdy+BEREd+DVYXcS1F/jdlstiKeB2NjY4/h+9n4+Dgxw+9nNpvtMZ4/QJxmxNaN'
'2isQbxXzwYWP9axETW8inwar1do0MzPTBWuwWCwy+M/F2vyEmshQCwX2YjH0KsnJyflRJpNJ8LySKx'
'/1VDK+TqerR54dqPHdc+fOmTIzM7twrxT5KlJTUzsPHjw4kJWV1VhRUSF9yceeUHLlo5YV2HuMX4ea'
't1+/ft2wb9++ycjISB1qUoY9URYWFjbC4/Fs0KQasZYwPnoR41dw5UPz5Yw/PDxci/zbwNcvlQ/tlH'
'PlY13LnPxq8FvR23RL5WPtyrjysY8V6GcSrVZbA3471n0kODh4Mjw8XI8+qIQ+lSEhITrGj4uLUzPd'
'FxQU3ETfkUCbCq586E3O+CaTqRm11yL3afAnAgICbP7+/jaBQGDj8/k2xvf19bUlJiZ2o2fKGB/akX'
'Plo6/InPyWpKQkE9jTQUFBr+SHhoYO1dfXlzI+tCN7DfwSJ78JfCP4Uy7wS7jyGxoaiqG/bGit6ezZ'
's0awGX/83/g4C7OhnWKufPQ0KePr9fpGnDNGkUg0CVsSH9qRcuVDz0XO/OuRv94VPtauiCsfWi5kfJ'
'w/KuTP+BNCoXBJ/MbGxkKu/Orq6gLoLxv7v/b8+fPawMBAxre/ih8bG9uPmsmhP8YveA38fMbHWcbO'
'4Hqc9204j5sLCwtbXvLRE1vxW4VvIRXer8DZk8/4qEM+Vz5qn8f46CWF+K6QQgdyfAMoEEPtbvB3ga'
'9QKOqw1tLy8vJ8jBzWfxkf2s3jysd5mtuSllY2mJzcZ4iOHrMKhb+NiURP+oODHVFr1xIztUjksPB4'
'T/Te3r9qBAJra2Rkt+rixTJoJ5cL/6JQqDDy+Y9Mnp6LpqCgBVNoKBkPHSLL8eOkPnOGDmzdSge8vK'
'hXLCbr/v1kFgjIsnMnGbdtWzC7uy8aebxHKSLRsr6/jmzf/qF+48YFY0gIDYOnB8+Ab03j0aNkOXWK'
'+k6fpgNbtlCUpyf1Hj5Mo4jNsnfvXxYYSFY+/7nezW0hZseO9S7iV4vd3P0njsSQ9cQx0sfGkhEMU0'
'wMmaOiCCInzbFjFOXhQVGbN5MaNfk73yoSkT0ijCbEMSR23+LPfLrAf2vlypVr5bv2zNsvfEmz312m'
'6W++psnPEsgujqXxI2IaT06iIdSFGbsePxRD9qhImjxzgqa/+pxmM9JoLOULku0KmGe+mE8X+G/C3v'
'XbtImf7uUzpPTxdfRERDqMSfE0k3aBZr9NpfkbGfQgJ+uFzd/IfHFv5lIKGRPjqDss3FH5id/iZS+f'
'Qd5Hmz5lvpw+XR2sZmv83l+3O9nD+4c0j20/33T3nitw2/pH0QavBel6T2LGrtk9ifvHc5c2e6vZu/'
'x1G3azuS7W/VXjDRj7D/WO0+d7sA9g65z2gfPeGuc7q5xz/vfjT4AtRCI='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["charging-20"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrNl1lQk1cUx3H6wIxOtQ8dH3TGVjZHMxACiMvIGtYCociM1kLYElEWhbLpICDVFhVFGMDKJqgIIc'
'O+75tAiIIsYQ0hJBAF6lJmrNZqCKfnZtKZPnQsIT70zvznu/Pd757fOeeee2+ipaVeq6ysDK2vr3/T'
'0NCwhk8gIv26urrfy8vLg7U+cQOATampqd+mp6dnZWdnD/T19UF/f/+/qre3F3Jych5lZGTcTktLY2'
'jKRi6zsLDwt6Kionc1NTWvOjo6lquqqtaGh4dhcnISpqenlSL9J0+eAI4p2tvbl8m3xcXF7+7du/cK'
'bZzYCBvj1UUb8qampoXx8XEhamp0dHQMbSpqa2uhs7MTcnNzoaCgAB4+fAiYe0Bf/8T8CAYHB6fQx5'
'nm5uYFYgPzsVtd/qVLlw50dXXJR0ZGlvE5jLEPdHd3D3C5XGFiYuKbsLAwRXBwMBCFh4cr8N1r5E9i'
'7ANYCwOtra3D6C+ZK8cxc3X5np6eOriGFVKp9PXU1NQ7jOflwMCAjM/nC3k83gTGLEDbQ21tbUPIEq'
'AmMPdCjF+Ga/GCzCFziQ1iayP8gICATKyrWsx1Fa7x4MzMzBy2FYlE8hZtf5ifnwci0heLxW9xfAX9'
'FGEd8jD3lY8fP65ls9mZmvAx1hrcY3kYTzOyWxcXF3tQLSKRiIv285FVgD5yMR9ljY2NnPz8/Nw7d+'
'5kY81m43iNpny0X43rmTcxMdG0sLDQjb6MRERECDGvPehTKa5xWUJCQh+TyRzJyspqxvOh6G8+rlW1'
'pvyenp5KFb+B8K9fvy50d3eX4dgY8suRX3706NFJKysrcVRUFK+6uprzD36lpnys+QrCHxsbq8d17k'
'pOTp5eLx/XrkITvr+/fwbWeDnmPBf5tcjvxPinGAzGevnlLBYrQxM+1n4Z4QsEAmX8V69enUT+gq+v'
'7zjuxyrcb1UeHh5KfmRkJA/vgtK7d+/mET7WTpmmfNzTpXje5QqFwnZkT2Dun7m5uS04OTmJ7e3txX'
'Q6XWxraysmfAsLC+JDH64Xl/BxT5R+Aj4XzzTC7wgNDZ3F2D/K9/LyGiY5wLMwG2uHqykfzzUlH8+W'
'tpCQEBHynyJ/fj18rB2N+S0tLRy813LxnmvDs16E7Keurq7r4mPtcDTl4/1XTPhY661BQUEzyJatl4'
'93VrGmfLRVpOI3I19I+C4uLkq+A/LtPsLH2inSmF9X96AjKal6Li9v+ryz82tvGk3OpFAU3rq6a0wd'
'HSAifS89PcUJAwN5rIXFylB8/Ej9hQtVWDsPNOGnuLpWzjo6vhWamMhlMTGKZHd3YB48CExzc/AyMQ'
'FvIhoNvKlUYBoaAvoF6VZWID12bFVoaCgXOTi8SWEwNnT/3rK1PSykUFan/fxg/PRpEF++DLM3bkB/'
'XBz0R0dDlY8PeBsbAxPZjV5e0I/fEc2FhMDc8eMgcnKCOVdXmNm7dzWTTj+kLr/Ims6SnQoAcfhZmP'
'D3h5nQUJgNCwNJZCRIw8Ohj80GppGRMu4hFgskvr5K7izmSOTsrJTUzwdkgQFw35LOUpefYnbYs8XR'
'Rb6UegV+RS0l/wTPEmJhIewMyM6EAj8wUBk70Qj+Bptn+YPUxxuengmCxbgYeJ7yMyylJEGLvbOc2N'
'oIv4Ri+uGRnSPw6PYwHsiG+Yux8DztGrzKvQWCzJvAcrRTaiYnA17eToXlaz+C9FwECLy9oNeKDjxr'
'OpTsM/mwEX4C1YyJfDnfxg5aKTTo2H8Y+HQHGPPwAMlpNgxFnQX2kYNw0uIQTF+IBulJPxhzcQb+EU'
'toNzSBpj1G0GdpQ/jyROp+bzXxn4fpU77n7KGt8tFGm5EptNMOQD/GI2AwQHKK9Z/85n3G0It9joHx'
'6lkDynfEphr8L7Zra+ve1zeSVFBM37co+ebAs7KFUazpObYfLF2Oh/mkeJBduQjPky6ClOUDY67fAN'
'/SGjqoZtCA/PK9tPeF+kbibVrausSmGnxt1HbzzdvMbuvsqy/WN5Jz9Kl/1FEPKHqsbGCI+IDxvoz9'
'AVbicD/gPhlluEGftQ3UU80VJfgtmfPLbkqtyeZtpsSWyqY6bRNqK2rnoa1bzc/t+Drm5i4DboGeob'
'BQn/qiGBkcPaM1ItIn78hYyi6DkvM7dkeTOWSuysYmDf8KfobagvpSZfMrFMmpnkq6qnc7Vd9sUc35'
'37e/ADYNPs0='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["charging-40"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrFlXtM01cUx8n+WCb7byYOH5ujQNQJRXyM+mCV8pglPAwgiJsWcLxVQFDRCYOYBZlQ5LnQhlJayv'
'tVXoI8pLwZjJSWl2VFAVFMQN0WTYTSs3NLZxY3Eyy43eSb3t7+7vdz7jnn/qqn93ajtLTUo6am5lld'
'XZ26trYWiLTzJ+Xl5a5672CkpKQcSE1NTeZwOB1tbW3q7u5u+DdJJBI1PtOOz7PT09P3r5bLZrPNeD'
'yeUiQSLVRVVc23trbOVlRULA0MDMDIyAiMjY1pROb9/f1QWVm5cOfOnVn8nMc9i7j31xs3bpjqyhcI'
'BI/Ra3ZwcFCBjLsymUyel5f3EtcAYwEulws5OTnQ3t4OJSUlkJ+f/3tXV5dcKpWOoRRisXiWz+fP6s'
'rHus739vbOdXR0yFpaWvowv31Y/9G4uLg/QkNDl4KDg4EoLCxMhWvPCgsL5ZinPuyPPoxP1tfXN4fz'
'OV354eHhguHh4UcKhWIBzz6HOZ7BOivwjMMkJmQMNDU1DTQ2NspQw5h7RWdn5wzWZ57swZw9jIiIEO'
'jK9/LyyigqKsptaGgoxDh60HNcqVQ+uX///nPUwuTkJBDdu3dvYWJi4jn2wjzGeBd7tL26uroQ65RL'
'PFbLRx8unv/W+Ph4w/T0dPPU1FQdsgQ9PT0czAEXJcQYhdgDvCwc2dnZWbiPuxZ8rOlf/Fo8ezPmoB'
'd7+xesa71cLi/CXJcgrzEhIaEV+6UiNzeX9y742M81mOcW7OchZ2fnBywWa2h0dLQM70aZg4ODgk6n'
'K/G+NuHzgrXkFxQU8LX8auQ3I1/+Jn5iYmLz3/n4ruCvFR/zLMb8N+E7Rebi4jL9P/Fb0HsQz/9GPv'
'4HFGCMa84fGhq6hf3d7+7u/oDwjxw5orSzs1Pa2NgoGQyGkvBtbW0VmZmZ9cXFxYK15uP9r4+Pjye9'
'N+Pk5DS1zLdHvu0rvpWVlTIqKqq9rKxMtJb8DEFpYXZtx7B3ZNzTvTbOi2Z0xyUKzV5taGkPRGRuRP'
'tqyWS/3YJT8NXH51MLfk7JyS/AWqyK7/S1n4AZnTNLPZeluixsVrEuxcMhRw+gOXiAuZ0bmNm6wec2'
'brDd2hVM6K5gaHUUaMEJQI8tWdxxJkvFjOE/cjnpp/P71zQo9Zknu1IdJmiHlHo5nI65CQccj8M+pg'
'fstHWH7Qx3MLF2A0O6G2z9clm00DSgJbaDUextsLhSqt4RkPJUVz4zuUXty+sAFrcNYiul4HWZDXuZ'
'nkC19wDj17iv841jm8D2ZhvYJTWrdeXToqtUPiIZ+FdMwLXbE+B1NR12M71gp70nUKzdkb8sMje2Pg'
'YmDA9wjM4Gv+JR8CtXwimhFGgxYpWufGpkkcriYj6YnOHA6exWCEkXA+NEMBz2DIADx/zA0oWl0cFj'
'/vjdHw4eDwHfjFvgkFgLxmeywOKiCIiHzvUPL9Twt/jehN3nOXAipQ5CcjrhrEgKpzIksIvJQnmDH1'
'8KPtn94JnaAozYEjDwToLNvskaPvHQlb8jNF9lfkGk4e88mwm0KwXA+KEOXNO6/8Fn/NgG1KhyoIRk'
'wwZWooZvfiEPiIeu/G1nRSriQfjbQjJg7yUR0K/VwtHUlfKFQDx05RuHCFXmkct8k6A0sMBYDsVVg/'
'PNrpXxI4VAPHTErzMK4i9RI5b5lIBUoEYIYP/3VeCY3LkiPjVCCMSDeL0l+wOUwWcBvKVdWH9KQBps'
'9cceCM+FL6LFwGR3wrfYb0cv52gUKBwC6wTJK/7H3olgiPHuwtwRD+Kl9Vzp+BC1meKTPmwcyF80Pc'
'cBi/NZsA/rT/JPznqKPwpBpTOoh/BNzggcvi4ByxgxMoVgFvoTmIZmgXEgb9HQO01GvLSeKx3voT56'
'/xOqqcGJpOotPtwFSiDnhUW4SG1N6p8kAS+OFAKLJlFTmrkTW4LvfTFQw4RqwwDOC7LHwCtJTDyIl9'
'bzrXuA5E7fyHLPeubF7za4X6/ceDJjeiMr67fNLO7Ljd5cINLMcY38tsE9vnI988IV/U9379HmfZ3e'
'6geJXR+1HrUJtRVFIVdEK4p2bZP2GX0dz/ufjz8BIXb1NQ=='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["charging-60"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrNl31Q0/cdx1371+66ddduU8Q7Nh8QKwhOEKpCQFFbwxIZkTLlQgpIkQYwBKtSMTqqyFMAI4aHxB'
'AwQAygAuHhwpOAqKCiPIugWLjtul6vd92sNirvfb4x89bbwxHwj33v3pdvvvl9X6/v4+8uCxbYVgwG'
'Q7LRaHxcX18/Q59gYfW6urrH1dXViQtec7lw4cKbOTk5oQqFQlVUVHS7u7sb165d+49hv7FnTp8+XZ'
'SVlRXK+s7HnZ2dnVhaWvr38vJyNt9vOjs7v7p8+TLu3r2L4eFhjI6OWjIyMoK+vj7U1NTMXLly5a+0'
'Jt+wPtT3bzR26Vzcubm5O8rKysytra2T5BobGhoa6e/vH9BqtTM0FlA7CgsLce7cOZATFy9eRElJiZ'
'nWYODmzZsjAwMDYy0tLZM0DrNcLv9gDnMXEddMrOn29vZbbW1tvfS9V6fT3T969Ojj2NjYmZiYGLDE'
'x8e/oLbvyD9C56CX1qG3qanpFpVpxqA1ENrqDwkJ8SRew/j4+Pe0vk9pfb/u6emZunr16j3KYEdHRz'
'+N63Zzc/Ntk8nUTxmi+j3aoyka89esD+tLe1DPWHPxi8ViBc2ltKurq4724Nb9+/cfTExMfPvw4cPv'
'JycnzY8ePQILq1P747GxsW/v3LkzQWO4TvOvaWhoKJVIJIp5+ksaGxs1dM6ayNE0PT3dNjU1VUfz09'
'24cUNF66ui8enoPJTR/dOq1eoCugMFNO8C8pfM119bW6sljobOXyPNu2VwcLAnLS2tn85dG7VdoH0x'
'0F1rl0ql1+kM1tB50/zTT+PWvkZ/Pa1zi16v7+fz+VP0+zCtQRXdxSoul3uPw+FMZGZmtlRWVpa8Tj'
'/ddy1xzpHf+ODBg+aKioq7PB7P6r9H/oH/6qd7Om//xUuXSk6qDA251e3DGVUdX32aWvDDGk7Ai1Vb'
'BDP8k3qwOHL4M8u8tr3YLkl7Epii/VKQfPb6wWxNTa3ROK/9DxAfq/c5rHm64aDGLNWYXshreyGVF8'
'Nrxy547vwYkflNCDvbBOcP9mA5JxC+B5TwT6uHU8L5Z47i/GebDmme8uNSjHPxb4xIilwtVj6PUZkg'
'1XXjjGkIh5UGePP+CI8Pg/GevwBOm4Owwi8ISzlBcPAJwm84Avgc0cEjvQMOySasPV6PlTHK55siks'
'Jt9fsfKy/gnbmCaG03RKpOnDIOIiIlH57cELhuC8Zy8v7W6v3XeMUr4JXZCYejJnhndSBA0Y5txyry'
'bfX7JhTIdshbn4svjeOTqjGktT5EZKoaHgG74bLtIyz1FZD/ZVh9md8uOG4JBjdZjXD9CIT6UcRUj4'
'MxGMtWv0+CUub1eZXZLVELt8+0SNT3IPKECj5/EGJjYBjW84Xw5IdZwuoePCHcA0IhOKZCUF4LnCRq'
'uEm1YAzGstm/XynzTDKYXROLYf9xFkKyqsHbfwprtwrgujUYzlt3w+1DkSWs7uQnwApvPjZGpWBdoh'
'q/DD0FV2kxGIOxbPVvisuTeRwymNdIX/p3ZVQiID4Vrv5Bs/avSSgGYzCWrf4N1Mf9oN7iXyzKROCp'
'SnDjTlr8Lv7/w//JiR/5GWPDHPxeYoVs7YEKs0uCxuLffrwMezIM8I/8HJsjZfAJl73ycyKOY5MoGZ'
'6hh7CDnlsVm2/xu0g0YAzGstkfo5C5SsstfruwDLgfUGNnRi32qroQpe1DeOH1V/69mj6EFvZip7yF'
'zpseduE5r/yMwVi2+tdH58rWJOjMzla/m6QI7x/Rwz+1EXzFtX/zb8noglvSZSyPK37ldyY/YzCWrX'
'73qFyZy/6X/kXkd44vwPqkCvidaADv9Oz9jMFYtvp/F5UtWx1favEvFKbDSazEukNl8Ekx4ve53bP2'
'MwZj2aj/iYvwi9T3YkvMqyUv/Y6fnqX3kA4bj9eBmzM7/+r9GjAGYzGmDf5frAo5Il8Zo33G3n9L6P'
'4vjT4Dl8Tz8JLVYru8C9HnhxCe12XJPt0ofNM6f+RfLMqwvP8Yg7EYc5buNyjv2nuHcB0i1eaVYs2M'
'c1we3BNVlv33/pMRm9M7ISgcgqh82pIgqvumddDY6rD2YBlcJYVwFudhpbh4hjF+7Rm4lTGt7NmUn1'
'IWveO3N3hx2NmxJRFFzxz3qZ6s+6wcm7+oAz+7E7tV/YjWf2nJbtUAeNTml0L+A2VYsU/9ZEl44fPF'
'wrzRdzhRuxjLyrSlsLG+TVnys/f3BLwbcDh14UfpzXZC5V/swgq+sxcV/WAnKgKLpU5ti4TKPy8MTj'
'exZ3++PoTL+loZbyyYX2H/I9+i/IpiT3GgLKMst2aZtc3e+sxb1j7/9+UfTexOOQ=='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["charging-80"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrFlXtQVNcdx22b/mGaP9KZdBpjlFiqbgR5KAQ1xYgRBCOyG9QQgrLgxvBalscaMIBEKYrAenktur'
'DssrxBXF2eIrgKG16igos8tFJHZpqZNpNpZxp0QODb87th6HTaTrmmMzkz39mz597f53Puuefeu2yZ'
'sFZfX3+yqanpaUtLy3xzczMoC/0po9GYuOz/3DQazc85jpPm5eWVsDbU09OD3t7e/xg6ptVqB+lclU'
'olpdof4s7Nzf2yoqLiaXV19Xetra3fdnZ2/sVkMsFqtWJsbAzj4+N8qD80NITGxsb5rq6uP7P1+Lay'
'snKqvLx8Kicn58QLug9VVVVN37hx4/Ho6OjDkZGRMeYdMRgM84wPs9mMoqIi6HQ6MCdoXsz3nK3B8O'
'3bt8eGh4cf3rx58zGb+zRjBQr1q9XqWHa90wMDA5PMdYfNY4D9H2COR8nJyU/lcvl8REQEKAqFYi41'
'NfXvbG7jbG8MNDQ0DLS1td1hbZLNbZrdjxih/oCAgB21tbXtExMTz9j6PhscHPzm1q1bk93d3Q9Y7j'
'OulV3f3Y6O63fb29ut165du89+H1gslkk252/Ymj2j2rq6ug5ivYg/Pj5e3dTcXK43Xe+7aO77U0vP'
'4FTv0Ojc0OjD+QePJvDkyRM+D/4wARrrHRqZM5r7p3QN5snCmsbe5ta28sTERPWL+n0V6R3ux/XTO5'
'IMMyequ+ZOG/uQf/UeUo23ISuxwC/fDE/ODL+CTvjk3sC2022wPVYHW0XZ7PoozczWBP20ODaj/UX8'
'W2XJxxyiL8zKdWYk1PQiv/0+1NfHUNg2hKCUfIhTSxCk64dY04edSQa4hGXBK+sqNmV0wialnY/zqV'
'aIoi7Mbj2SpBTq33PmUr1Y3YkwQw9k+m6kNVpx9uoo4guNcPY+CJF3ECSafuw93w+RXzhstvtji6IA'
'W7Iti353VSd8829ib/qli0L9PimlnKSwezbaNIEI40Okd/wRmdcfI+a8CY7eAVjnfRjiogE+dpJI2H'
'rsx/sJRThosMK35B4O1Ywj6sojSNRfzRJLqH93kp7bkd4046w0wO2LSsRU9SOrYwKpZW1w/zAY7gHh'
'ON35NR9PWQJc9x1CUEYlZFVWvHemBRvidKBaYhBLqN/ruJ7bntY44xRfijWfcvjwbD3iDF04mlkJZ6'
'+P4Owrg6KOrQeL28FYiNj1e8jPwTv9ClbL8vFaUAaolhjEEurflVDCuZ8yzTjG6/GW7ByC8xogyyiD'
'q89HvN/eMxBOPlI+1Cf/2u1ivBeVjXeO6Xg/1RKDWML9xdzvTl5Z9IfkN+LA5yps3n0ATv/N7+6Hd4'
'ISsC3RsOgnBrGE+ncoi7ltKZdnHOL0sGH+Qzkm+Cuzsclr/5L9VEsMYgn1b1dquC3JRt6/+ogKgdxl'
'SJRZbO33w9Hz4JL9xCCWYH/cBc4t6RLvXxWqQoDKCHEc83sK8xODWEL97jEXONfEizMbY3V4MzQbB7'
'LqsS82E467/OGwa2l+qiUGsYT631UUcpsT6nj/ypBsSDLq8YEig/dvFOAnBrEEv//lBZzz5zUz9ozx'
'hjQLe9JqEJBWBjeJDK6So3CRhC/6N4vD4MTeB/Z7grEzrgAuyu+fP6olBrGE+t0iCzgn5T/9ruyZDs'
'y9iqjSPkRV3sPR0qFFf3ChBaG6O/iYfS+8fn958f1DtcQgllC/a3ge5xBXteh3ii3GtuQ67DrThn15'
'vf/m332uG05fmLA22oA3QnN5v12MDsQgllC/y2d53MbY7/0rgjNhr9Cw70AtPNJbsTd36X5iEEuof9'
'PRHM4upoL3v878oqjzcEmsZu/zFuzhepbsJwaxBOp/6hCSUbghuuI5MX59+CzWRRayvVSFd082w0eA'
'nxjEIuYS3T9h+aVd4EmtSF7+nL5hb7Ln7zdhBXBQVmJLahN2q75CWMUIxMf1fD7V3WVj/+rn9wyrFc'
'nLnm/4+MsSYi6w/1d7ieW19d6yIzaykjlRtA72cjU2x2v5++9+qgXvZ1pwuHQM4fVfI4Il2DAOj8wu'
'fm6bEqrhGFMMuyg13pbrQIz1XtJQYi6wl9J+wbLCzjfyxJqQgr+tOlI0ZxepnXU5VoOd7P775VgQqL'
'UirHYS4SyB2mHs4yzwSGN+ZRVEEdpZqlkjzfvr2x+EpRCL5WWBe+BnLK8uX7589VpPadg6/8Q620/O'
'jtsEq79bJdVMrwzRzK2QFoNCfRpbzY7ROev8j9dSDdUSY4H1Qxqt2yssv2JZyfIWiy3LbxdiuzC2cu'
'GcVwSs9Y/a/gGJTEv0'
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 32, 32, 128)

images["charging-100"] = gtk.gdk.pixbuf_new_from_data(zlib.decompress(base64.b64decode(
'eNrt1VtI01EcB3AfgqJCp+bUZd7SDDLKYhMREU2dZmnTUMtSl3kJajp1sR60RfMeCZO52UynbuWmf4'
'c2s1ZmQ0O7aOVIX8qHNCQphrJ8KPDbf0J7CXE6goIOHM7D+Z3POZzfudjZrV2USqWXVqtNIus5giCy'
'FQoFSyaTedjZUOy5aidnjirTi9d9hy3qmefJdQu85geL/NbHi/z2Jwtn6jSfXYsIpXNBR4bDBaXjem'
'xXrppPK+40BlcPmJgNz5El0yNbPgp22zjOKgxIlk8guGoA9mUjoPAfmZwLO42OHNVla2w/0f3N/gId'
'ElveIrFtCpFNk4iQjCNKNIzwOj0YtXocqBnCzspX2FZuWKkU4Rj8rurgLRjcYs0c9Bt6JN8zgtnxCY'
'zb0/CumwSl0mDxVsyqd/Ct/wCGfAasXiMO1+hh9f7kSEDl9eCQ6DUilTM4pvmKBNIwt/HdXxBPzIOp'
'mgO9YQJ7hE9Bu9IH85j1+I6FXfAo1SFUPIZU9TQyeudwpHkKIaIXiKofRmj1Q3gUKrA1vx0u5Fo26r'
'NaDQg8eQkH07hIU70n8/IGu+PY8IpKB12gsdlPIn1/Zjr2Hs3EKdI/QebdM4wFWshxMK793X7A9YE1'
'fWrB3Q37MeIR7E8rsfj+MenwiUix+J7MLAQLtRv24ySj8I1MXdWn0mMRWtH33/9HffP5CUzhrurvis'
'6w6fzsqxhEQssETndNg62ds/ix4mcIr9UhqIwArUhl0/0NqBlFkGTyN9+Bo8Imtszm9+FP+NSchh9O'
'BWqrfWpxN+lLv1vvi0tcc6Xf3DjtSz6lfQi8+RKZmo/w++XXD4FyUYnt2Y1wyr21ZI51z5MWreePp+'
'U27qDmSEvc8mT97vmy2TCB2hRdTixHC4nlsFKVieybdTkv7TfHmGNXc34ChMzGeQ=='
    )), gtk.gdk.COLORSPACE_RGB, True, 8, 24, 24, 96)

for step in [80, 60, 40, 20, 0]:
  for i in range(step, 100):
    if str(i) not in images:
      images[str(i)] = images[str(step)]
    if "charging-" + str(i) not in images:
      images["charging-" + str(i)] = images["charging-" + str(step)]

images["present"] = images["100"]

if __name__ == '__main__':
  BatteryMonitor()
  gtk.main()
