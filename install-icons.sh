#!/bin/sh

set -x
set -e

DEST="$HOME/.local/share/icons/hicolor/scalable/status"

mkdir -p "$DEST"

LAST=""
for PREFIX in "pbm-charging" "pbm"
do
  for i in $(seq 0 100)
  do
    ICON="$PREFIX-$i.svg"
    if [ -f "$ICON" ]
    then
      cp "$ICON" "$DEST"
      LAST="$ICON"
    else
      cp "$LAST" "$DEST/$ICON"
    fi
  done
done

ICON="pbm-present"
if [ -f "$ICON" ]
then
  cp "$ICON" "$DEST"
  LAST="$ICON"
else
  cp "$LAST" "$DEST/$ICON"
fi
