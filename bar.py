#!/usr/bin/env python2

import gtk
import math

class Bar(gtk.Window):
  def __init__(self, monitor):
    super(Bar, self).__init__()
    self.bar_width = 2
    self.monitor = monitor
    self.set_decorated(gtk.FALSE)
    self.set_keep_above(gtk.TRUE)
    self.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_SPLASHSCREEN)
    self.connect("destroy", gtk.main_quit)
    area = gtk.DrawingArea()
    area.connect("expose-event", self.expose)
    self.add(area)
    self.show_all()
    self.reposition()

  def reposition(self):
    g = self.get_screen().get_monitor_geometry(self.monitor)
    self.set_gravity(gtk.gdk.GRAVITY_NORTH_WEST)
    self.resize(g.width, self.bar_width)
    self.move(g.x, g.y + g.height - self.bar_width)
    self.g = g

  def expose(self, widget, event):
    self.reposition()
    cr = widget.window.cairo_create()
    # bg
    cr.set_source_rgb(0.1, 0.1, 0.1)
    cr.rectangle(0, 0, self.g.width, self.bar_width)
    cr.fill()
    # fg
    cr.set_source_rgb(0.9, 0.4, 0.4)
    cr.rectangle(0, 0, self.g.width * 0.42, self.bar_width)
    cr.fill()

bars = []
for monitor in range(gtk.gdk.get_default_root_window().get_screen().get_n_monitors()):
  bars.append(Bar(monitor))
gtk.main()
